p_text = 0;
p_stranka = 1;
n_datum = 0;
n_text = 1;
/*menu=[
  {label:"Home", target:"index"},
  {label:"topic 1", target:"topic 1"},
  {label:"topic 2", target:"topic 2"}
];*/

function ahref(str,txt){
  document.write("<a href=\"");
  document.write(str+".html\"");
  document.write(">"+txt+"</a>");
}

function br(){
  document.write("<br>");
}

function start_page(root){
  document.write("<head>");
  document.write("<title>Hume2's Gallery</title>");
  document.write("<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">");
  document.write("<meta name=\"descreption\" content=\"Hume2's gallery of miscelaneous stuff.\">");
  document.write("<meta name=\"author\" content=\"Hume2\">");
  document.write("<link rel=stylesheet href=\""+root+"style.css\" type=\"text/css\" title=\"styl\">");
  document.write("</head><body><br>");
  document.write("<center><img src=\""+root+"style/logo.png\" alt=\"Hume2's Gallery\" title=\"logo\" width=\"50%\" margins=-10></center><br>");
  //document.write("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>");
  document.write("<table width=\"75%\" class=main align=center>");
  
/*  document.write("<tr>");
  size = 0;
  for (var i in menu){
    document.write("<th class=menu background=\""+root+"style/bg3.png\">");
    ahref(root + menu[i].target, menu[i].label);
    document.write("</th>");
    size++;
  }
  document.write("</tr>");*/
  size=1;

  document.write("<tr><td class=main colspan="+size+" background=\""+root+"style/bg3.png\">");
  
}

function end_page(){
  document.write("</td></tr></table></body><br>");
}

function image(txt, img, mini, width="200px") {
	if (typeof mini == "undefined") {
		mini = img;
	}

	document.write("<table class=image width=\"" + width + "\"><tr><td>");

	document.write("<a href=\"" + img + "\">");
	
	document.write("<img src=\"" + mini + "\" width=\"" + width + "\">");
	document.write("</a><br><small>");
	document.write(txt);
	document.write("</small></td></tr></table>");
}

function galery(images, height) {
	document.write("<center><br>");
	for (var i in images) {
		if (typeof images[i].mini == "undefined") {
			images[i].mini = images[i].img;
		}
		document.write("<a href=\"" + images[i].img + "\">");
		document.write("<table class=\"galery\" width=\"" + height + "\">");
		document.write("<tr><td align=center valign=middle height=\"" + height + "\">");
		document.write("<img src=\"");
		document.write(images[i].mini);
		document.write("\" style=\"max-height:" + height + "; max-width:" + height + ";\"></td></tr><tr><td><center>");
		document.write(images[i].txt);
		document.write("</center></td></tr></table></a>");
	}
	document.write("<br clear=all></center>");
}


function locations(data) {
	document.write("<table width=100% border=1 style=\"border-collapse: collapse\">");
	document.write("<tr><th>Name</th><th>Owner</th><th>X</th><th>Z</th></tr>");
	for (var i in data) {
		document.write("<tr><th colspan=4><font color=#CD5932>" + data[i].city + "</font></th></tr>");
		for (var j in data[i].items) {
			document.write("<tr><td>" + data[i].items[j].label + "</td>");
			document.write("<td>" + data[i].items[j].owner + "</td>");
			document.write("<td>" + data[i].items[j].x + "</td>");
			document.write("<td>" + data[i].items[j].z + "</td></tr>");
		}
	}
	document.write("</table>");
}

function icons(menu, height, root) {
	document.write("<center><br>");
	for (var i in menu) {
		document.write("<a href=\"" + menu[i].href + "\">");
		document.write("<table class=\"icons\" width=\"" + height + "\">");
		document.write("<tr><td align=center valign=middle height=\"" + height + "\">");
		document.write("<img src=\"");
		if (menu[i].img == "") {
			document.write(root + "style/talogo3.png");
		} else {
			document.write(menu[i].img);
		}
		document.write("\" style=\"max-height:" + height + "; max-width=" + height + ";\"></td></tr><tr><th>");
		document.write(menu[i].title);
		document.write("</td></tr></table></a>");
	}
	document.write("<br clear=all></center>");
}

function clickable(src) {
	document.write("<br><a href=\""+src+"\"><img class=clickable src=\""+src+"\"></a><br><br>")
}
